[![pipeline status](https://gitlab.com/melnichenkoviacheslav/general-extensions/badges/master/pipeline.svg)](https://gitlab.com/melnichenkoviacheslav/general-extensions/-/commits/master)
# GeneralExtensions
General extensions that called to solve variety of base extensions at IA domain.


### General types
- Object
- String
- Enum
- Bool

### General classes
- Action
- Wait
